package com.example.examjanuary.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.examjanuary.R


class Fragment1 : Fragment(),View.OnClickListener {

    lateinit var email: EditText
    lateinit var password: EditText
    lateinit var loginbtn: Button
    lateinit var txView: EditText


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val root:View = inflater.inflate(R.layout.fragment_2, container, false)
        loginbtn=root.findViewById(R.id.loginbtn)

        loginbtn!!.setOnClickListener(this)

        return root

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        email = view.findViewById(R.id.emailtxt)
        password = view.findViewById(R.id.passwordtxt)
        txView=view.findViewById(R.id.textView)


    }


    override fun onClick(v: View?) {

        var emailtxt:String=email.text.toString()
        var passwordtxt:String=password.text.toString()
        var textView:String=txView.text.toString()


        if (emailtxt=="manolo" && passwordtxt=="123456"){
            txView.text="Login Successfull"

        }
        else{
            txView.text="Login Failed"

        }

    }
}



